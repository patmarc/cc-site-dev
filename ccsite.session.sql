-- SELECT t.title,
--     g.name
-- FROM track t
--     JOIN track_genre tg ON t.track_id = tg.track_id
--     JOIN genre g ON g.genre_id = tg.genre_id;
-- How many track belongs to genre techno
SELECT COUNT(t.title) AS techno_tracks
FROM track t
    INNER JOIN track_genre tg ON t.track_id = tg.track_id
    INNER JOIN genre g ON g.genre_id = tg.genre_id
WHERE g.name = 'techno';
SELECT t.track_id,
    t.audio_url,
    t.title,
    t.description,
    g.name AS genre
FROM track t
    JOIN track_genre tg ON t.track_id = tg.track_id
    JOIN genre g ON g.genre_id = tg.genre_id;
SELECT t.track_id,
    t.audio_url,
    t.title,
    t.description,
    t.genre_id,
    t.duration_id,
    t.tempo_id,
    d.duration_id,
    d.time as duration,
    te.tempo_id,
    te.value as tempo,
    tg.tag_id,
    tg.name as tag_name
FROM track t
    INNER JOIN tag tg on t.tag_id = tg.tag_id
    INNER JOIN duration d on t.duration_id = d.duration_id
    INNER JOIN tempo te on t.tempo_id = te.tempo_id;
-- Update all tag_names to lowercases:
UPDATE tag
SET name = LOWER(name::TEXT)::TEXT [];
-- Display tag_name array to lowercases:
SELECT LOWER(name::TEXT)::TEXT []
FROM tag;
-- Display tracks titles and their authors:
select t.title,
    a.name
from track t
    join author a on t.track_id = a.author_id;
-- ddd
'SELECT t.track_id,t.audio_url,t.title,t.description,t.author_id,t.genre_id,t.duration_id,t.tempo_id,a.author_id,a.name as author_name,d.duration_id,d.time as duration,te.tempo_id,te.value as tempo,tg.tag_id,tg.name as tag_name FROM track t INNER JOIN author a on t.author_id = a.author_id INNER JOIN tag tg on t.tag_id = tg.tag_id INNER JOIN duration d on t.duration_id = d.duration_id INNER JOIN tempo te on t.tempo_id = te.tempo_id;'

-- Favorite
SELECT track_id from favorite where user_id = $1, [req.query.id];

