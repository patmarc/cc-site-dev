-- CREATE TABLE track (
--     id BIGSERIAL NOT NULL PRIMARY KEY,
--     audio_url TEXT NOT NULL,
--     title TEXT NOT NULL,
--     description TEXT NOT NULL,
--     author TEXT NOT NULL,
--     duration DECIMAL NOT NULL,
--     tempo SMALLINT NOT NULL,
--     genre TEXT NOT NULL,
--     tags TEXT NOT NULL,
--     license_name VARCHAR(255) NOT NULL,
--     FOREIGN KEY (license_name) REFERENCES licenses(id)
-- );
CREATE TABLE licenses (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    license_name VARCHAR(255) NOT NULL,
);
CREATE TABLE track (
    track_id BIGSERIAL NOT NULL PRIMARY KEY,
    audio_url TEXT NOT NULL,
    title TEXT NOT NULL,
    description TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- genre_id INT NOT NULL,
    -- duration_id BIGINT NOT NULL,
    -- tempo_id BIGINT NOT NULL,
    -- tag_id BIGINT NOT NULL,
    -- author_id BIGINT NOT NULL,
    -- license_id INT NOT NULL
);
CREATE TABLE duration (
    duration_id BIGSERIAL NOT NULL PRIMARY KEY,
    time DECIMAL NOT NULL
);
CREATE TABLE tempo (
    tempo_id BIGSERIAL NOT NULL PRIMARY KEY,
    value SMALLINT NOT NULL
);
CREATE TABLE author (
    author_id BIGSERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    contact TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
);
CREATE TABLE license (
    license_id SERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL
);
alter table track
add column tag_id bigserial;
alter table track
add constraint fk_tag_id foreign key (tag_id) references tag(tag_id);
alter table track
add column duration_id bigserial;
alter table track
add constraint fk_duration_id foreign key (duration_id) references duration(duration_id);
alter table track
add column tempo_id bigserial;
alter table track
add constraint fk_tempo_id foreign key (tempo_id) references tempo(tempo_id);
-- Create an author_id column with a foreign key (author_id) in track table:
ALTER TABLE track
ADD COLUMN author_id BIGSERIAL NOT NULL;
alter table track
add constraint fk_author_id foreign key (author_id) references author(author_id);
CREATE TABLE favorite (
    favorite_id BIGSERIAL NOT NULL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    track_id INTEGER NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
);
--NextAuth starts:
CREATE TABLE accounts (
    id SERIAL,
    compound_id VARCHAR(255) NOT NULL,
    user_id INTEGER NOT NULL,
    provider_type VARCHAR(255) NOT NULL,
    provider_id VARCHAR(255) NOT NULL,
    provider_account_id VARCHAR(255) NOT NULL,
    refresh_token TEXT,
    access_token TEXT,
    access_token_expires TIMESTAMPTZ,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
CREATE TABLE sessions (
    id SERIAL,
    user_id INTEGER NOT NULL,
    expires TIMESTAMPTZ NOT NULL,
    session_token VARCHAR(255) NOT NULL,
    access_token VARCHAR(255) NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
CREATE TABLE users (
    id SERIAL,
    name VARCHAR(255),
    email VARCHAR(255),
    email_verified TIMESTAMPTZ,
    image TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
CREATE TABLE verification_requests (
    id SERIAL,
    identifier VARCHAR(255) NOT NULL,
    token VARCHAR(255) NOT NULL,
    expires TIMESTAMPTZ NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
CREATE UNIQUE INDEX compound_id ON accounts(compound_id);
CREATE INDEX provider_account_id ON accounts(provider_account_id);
CREATE INDEX provider_id ON accounts(provider_id);
CREATE INDEX user_id ON accounts(user_id);
CREATE UNIQUE INDEX session_token ON sessions(session_token);
CREATE UNIQUE INDEX access_token ON sessions(access_token);
CREATE UNIQUE INDEX email ON users(email);
CREATE UNIQUE INDEX token ON verification_requests(token);
--NextAuth ends: