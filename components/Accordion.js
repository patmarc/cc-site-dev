const Accordion = ({
  title1,
  filter1,
  title2,
  filter2,
  title3,
  filter3,
  title4,
  filter4,
  title5,
  filter5,
}) => {
  return (
    <div className='accordion accordion-flush ps-2' id='accordionFlushExample'>
      <div className='accordion-item'>
        <h2 className='accordion-header' id='flush-headingOne'>
          <button
            className='accordion-button collapsed'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#flush-collapseOne'
            aria-expanded='false'
            aria-controls='flush-collapseOne'
          >
            {title1} <div className='ps-3'>Genre</div>
          </button>
        </h2>
        <div
          id='flush-collapseOne'
          className='accordion-collapse collapse'
          aria-labelledby='flush-headingOne'
          data-bs-parent='#accordionFlushExample'
        >
          <div className='ps-4'> {filter1}</div>
        </div>
      </div>
      <div className='accordion-item'>
        <h2 className='accordion-header' id='flush-headingTwo'>
          <button
            className='accordion-button collapsed'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#flush-collapseTwo'
            aria-expanded='false'
            aria-controls='flush-collapseTwo'
          >
            {title2} <div className='ps-3'>Tags</div>
          </button>
        </h2>
        <div
          id='flush-collapseTwo'
          className='accordion-collapse collapse'
          aria-labelledby='flush-headingTwo'
          data-bs-parent='#accordionFlushExample'
        >
          <div className='pt-3 pb-4 px-2'>{filter2}</div>
        </div>
      </div>
      <div className='accordion-item'>
        <h2 className='accordion-header' id='flush-headingThree'>
          <button
            className='accordion-button collapsed'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#flush-collapseThree'
            aria-expanded='false'
            aria-controls='flush-collapseThree'
          >
            {title3} <div className='ps-3'>Author</div>
          </button>
        </h2>
        <div
          id='flush-collapseThree'
          className='accordion-collapse collapse'
          aria-labelledby='flush-headingThree'
          data-bs-parent='#accordionFlushExample'
        >
          <div className='pt-3 pb-4 px-2'>{filter3}</div>
        </div>
      </div>
      <div className='accordion-item'>
        <h2 className='accordion-header' id='flush-headingFour'>
          <button
            className='accordion-button collapsed'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#flush-collapseFour'
            aria-expanded='false'
            aria-controls='flush-collapseFour'
          >
            {title4} <div className='ps-3'>Duration</div>
          </button>
        </h2>
        <div
          id='flush-collapseFour'
          className='accordion-collapse collapse'
          aria-labelledby='flush-headingFour'
          data-bs-parent='#accordionFlushExample'
        >
          <div className='pt-4 pb-5 px-4'>{filter4}</div>
        </div>
      </div>
      <div className='accordion-item'>
        <h2 className='accordion-header' id='flush-headingFive'>
          <button
            className='accordion-button collapsed'
            type='button'
            data-bs-toggle='collapse'
            data-bs-target='#flush-collapseFive'
            aria-expanded='false'
            aria-controls='flush-collapseFive'
          >
            {title5} <div className='ps-3'>Tempo</div>
          </button>
        </h2>
        <div
          id='flush-collapseFive'
          className='accordion-collapse collapse'
          aria-labelledby='flush-headingFive'
          data-bs-parent='#accordionFlushExample'
        >
          <div className='pt-4 pb-5 px-4'>{filter5}</div>
        </div>
      </div>
    </div>
  );
};

export default Accordion;
