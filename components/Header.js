import headerStyles from '../styles/Header.module.css';

const Header = () => {
  return (
    <div className='m-5'>
      <h1 className={headerStyles.title}>
        <span>Music</span> Free
      </h1>
      <p className={headerStyles.description}>Free background music for videos and podcasts</p>
    </div>
  );
};

export default Header;
