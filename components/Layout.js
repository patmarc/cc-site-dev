import Navigation from '@/components/layout/navigation';
import Meta from './Meta';
import Head from 'next/head';
import Header from './Header';
import styles from '../styles/Layout.module.css';
import { tw } from 'twind';
import { StoreProvider } from '@/store/Store';

const Layout = ({ children }) => {
  return (
    <>
      <Meta />
      <Head>
        <title>CCSite</title>
        <link rel='icon' href='/favicon.ico' />
        <link
          href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css'
          rel='stylesheet'
          integrity='sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl'
          crossOrigin='anonymous'
        ></link>
        <script
          async
          defer
          src='https://code.jquery.com/jquery-3.5.1.min.js'
          integrity='sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0='
          crossOrigin='anonymous'
        ></script>
        <script
          async
          defer
          src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js'
          integrity='sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi'
          crossOrigin='anonymous'
        ></script>
        <script
          async
          defer
          src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js'
          integrity='sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG'
          crossOrigin='anonymous'
        ></script>
      </Head>
      <StoreProvider>
        <Navigation />
      </StoreProvider>
      <div className={styles.container}>
        <main className={styles.main}>
          {/* <Header /> */}
          {children}
        </main>
      </div>
    </>
  );
};

export default Layout;
