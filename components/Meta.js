import Head from 'next/head';

const Meta = ({ title, keywords, description }) => {
  return (
    <Head>
      <meta name='viewport' content='minimum-scale=1 width=device-width, initial-scale=1' />
      <meta name='keywords' content={keywords} />
      <meta name='description' content={description} />
      <meta name='robots' content='index, follow' />
      <meta charSet='utf-8' />
      <link rel='icon' href='/favicon.ico' />
      <title>{title}</title>
    </Head>
  );
};

Meta.defaultProps = {
  title: 'CCSite',
  keywords: 'free music, free background music',
  description: 'Completely Free Music For Your videos, Podcasts, Games, Applications.',
};

export default Meta;
