import { tw } from 'twind';
import ReactModal from 'react-modal';
import { useStore } from '@/store/Store';
import { GrClose } from 'react-icons/gr';
import Rating from 'react-rating';

const ModalRating = () => {
  const { modalRatingOpen, setModalRatingOpen, ratingComment, setRatingComment } = useStore();

  const handleRatingMsg = (event) => {
    setRatingComment(event.target.value);
  };
  const closeModal = () => {
    console.log(ratingComment);
    setModalRatingOpen(false);
    setRatingComment('');
  };

  const customStyles = {
    content: {
      top: '40%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      padding: '7px',
      borderRadius: '4px',
      transform: 'translate(-50%, -50%)',
      width: '25%',
    },
  };

  ReactModal.setAppElement('body');

  return (
    <ReactModal
      isOpen={modalRatingOpen}
      onRequestClose={closeModal}
      closeTimeoutMS={500}
      style={customStyles}
    >
      <button onClick={closeModal} className={tw('float-right text-xl text-black')}>
        <GrClose />
      </button>
      <div className={tw('flex flex-col py-3 text-center')}>
        <div className={tw('')}>
          <div className={tw('flex flex-col')}>
            <div className={tw('pb-0')}>
              <h2 className={tw('text-gray-800 text-lg font-semibold')}>Rate this music!</h2>
            </div>
            <div className={tw(' w-full flex flex-col ')}>
              <div className={tw('flex flex-col  py-2 space-y-3 mx-auto')}>
                <span className={tw('text-lg text-gray-800')}>How do you like this music?</span>
                <div className={tw('flex mx-auto py-2 text-yellow-400')}>
                  <Rating
                    emptySymbol='far fa-star fa-2x'
                    fullSymbol='fas fa-star fa-2x'
                    fractions={2}
                  />
                </div>
              </div>
              <div className={tw('md:w-3/4 flex flex-col mx-auto')}>
                <textarea
                  rows='3'
                  className={tw('p-4 mb-2 text-gray-500 resize-none border-1 border-gray-300')}
                  value={ratingComment}
                  onChange={handleRatingMsg}
                  placeholder='Leave a comment, if you want'
                ></textarea>
                <button
                  className={tw(
                    'inline-block px-6 py-2 text-xs font-medium leading-6 text-center text-white uppercase transition bg-gradient-to-r from-purple-500 to-indigo-500 hover:bg-indigo-600 hover:bg-purple-500 focus:outline-none'
                  )}
                  onClick={() => closeModal()}
                >
                  Rate now
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </ReactModal>
  );
};

export default ModalRating;
