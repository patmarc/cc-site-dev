import { tw } from 'twind';
import ReactModal from 'react-modal';
import { useStore } from '@/store/Store';
import { GrClose } from 'react-icons/gr';
import {
  GoogleLoginButton,
  FacebookLoginButton,
  TwitterLoginButton,
} from 'react-social-login-buttons';

ReactModal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.75)';

const ModalSignIn = () => {
  const { modalSignInOpen, setModalSignInOpen } = useStore();

  const openModal = () => {
    setModalSignInOpen(true);
  };
  const closeModal = () => {
    setModalSignInOpen(false);
  };

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      transform: 'translate(-50%, -50%)',
      padding: '0',
      margin: '0',
      width: '25%',
    },
  };

  const socialBtnStyle = { boxShadow: 'none' };

  ReactModal.setAppElement('body');

  return (
    <>
      <ReactModal
        isOpen={modalSignInOpen}
        onRequestClose={closeModal}
        style={customStyles}
        closeTimeoutMS={500}
      >
        <div className={tw('min-w-screen flex items-center justify-center')}>
          <div
            className={tw(
              'md:flex w-full bg-gray-100 text-gray-500 shadow-xl w-full overflow-hidden'
            )}
          >
            <div className={tw('w-full py-8 px-5 md:px-10')}>
              <button onClick={closeModal} className={tw('float-right ')}>
                <GrClose />
              </button>
              <div className={tw('text-center mb-3')}>
                <h1 className={tw('font-bold text-3xl text-gray-900 uppercase')}>Login</h1>
                <div class='msg'>
                  <div className='p-1'>
                    <GoogleLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                  </div>
                  <div className='p-1'>
                    <TwitterLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                  </div>
                  <div className='p-1'>
                    <FacebookLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                  </div>
                </div>
              </div>
              <div>
                <div className={tw('flex -mx-3')}></div>
                <div className={tw('flex -mx-3')}>
                  <div className={tw('w-full px-3 mb-3')}>
                    <label for='' className='text-xs font-semibold px-1'>
                      Email
                    </label>
                    <div className={tw('flex')}>
                      <div
                        className={tw(
                          'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                        )}
                      >
                        <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                      </div>
                      <input
                        type='email'
                        className={tw(
                          'w-full -ml-10 pl-10 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                        )}
                        placeholder=''
                      />
                    </div>
                  </div>
                </div>
                <div className={tw('flex -mx-3')}>
                  <div className={tw('w-full px-3 mb-3')}>
                    <label for='' className={tw('text-xs font-semibold px-1')}>
                      Password
                    </label>
                    <div className={tw('flex')}>
                      <div
                        className={tw(
                          'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                        )}
                      >
                        <i className={tw('mdi mdi-lock-outline text-gray-400 text-lg')}></i>
                      </div>
                      <input
                        type='password'
                        className={tw(
                          'w-full -ml-10 pl-10 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                        )}
                        placeholder=''
                      />
                    </div>
                  </div>
                </div>
                <div className={tw('mb-3 ml-2 text-sm')}>
                  <a href='#'>Forgot your password?</a>
                </div>
                <div className={tw('flex -mx-3')}>
                  <div className={tw('w-full px-3 mb-5')}>
                    <button
                      className={tw(
                        'block w-full max-w-xs mx-auto bg-gradient-to-r from-purple-500 to-indigo-500 text-white border-indigo-500 hover:bg-indigo-600 rounded-lg px-3 py-3 font-semibold uppercase'
                      )}
                    >
                      Log in
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <iframe src='/signin' height='620px' width='490px' />
      </ReactModal>
    </>
  );
};

export default ModalSignIn;
