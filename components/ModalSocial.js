import { useState } from 'react';
import ReactModal from 'react-modal';
import { useStore } from '@/store/Store';
import { FacebookShareButton, TwitterShareButton } from 'react-share';
import styles from '@/styles/ModalSocial.module.css';
import { FaTwitter, FaFacebookF } from 'react-icons/fa';
import { AiOutlineMail } from 'react-icons/ai';
import { IoLink } from 'react-icons/io5';
import { GrClose } from 'react-icons/gr';
import { CopyToClipboard } from 'react-copy-to-clipboard';

ReactModal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.75)';

const ModalSocial = () => {
  const { modalSocialOpen, setModalSocialOpen, modalSocialData } = useStore();
  const [copy, setCopy] = useState(false);
  const msg = 'Check out this track named ';
  const baseUrl = 'http://localhost:3000';
  const fullUrl = `${baseUrl}/tracks/${modalSocialData[0]}`;
  const title = `${msg}${modalSocialData[1]}! `;

  const openModal = () => {
    setModalSocialOpen(true);
  };
  const closeModal = () => {
    setModalSocialOpen(false);
    setCopy(false);
  };

  ReactModal.setAppElement('body');
  return (
    <>
      <ReactModal
        isOpen={modalSocialOpen}
        onRequestClose={closeModal}
        // style={customStyles}
        closeTimeoutMS={500}
        className='Modal'
      >
        <div className={styles.container}>
          <button onClick={closeModal} className={styles.cross}>
            <GrClose />
          </button>
          <div className={styles.header}>Share this music track </div>
          <ul>
            <li className={styles.socialShare}>
              <TwitterShareButton url={fullUrl} title={title}>
                <div className={styles.twitter}>
                  <FaTwitter /> twitter
                </div>
              </TwitterShareButton>
            </li>
            <li className={styles.socialShare}>
              <FacebookShareButton url={fullUrl} quote={title}>
                <div className={styles.facebook}>
                  <FaFacebookF /> facebook
                </div>
              </FacebookShareButton>
            </li>
            <li className={styles.socialShare}>
              <CopyToClipboard text={`${title}${fullUrl}`} onCopy={() => setCopy(true)}>
                <div className={styles.clipboard}>
                  <IoLink /> {copy ? 'link copied!' : 'copy link'}
                </div>
              </CopyToClipboard>
            </li>
          </ul>
        </div>
      </ReactModal>
    </>
  );
};

export default ModalSocial;
