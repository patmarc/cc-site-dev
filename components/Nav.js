import Link from 'next/link';
import navStyles from '../styles/Nav.module.css';
import Image from 'next/image';

const Nav = () => {
  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='col-2 align-self-center'>
          <Image src='/cc-logo.png' alt='Picture of the author' width={300} height={60} />
        </div>
        <div className='col-10'>
          <nav className={`${navStyles.nav} `}>
            <ul>
              <li>
                <Link href='/'>Home</Link>
              </li>
              <li>
                <Link href='/browse'>Browse</Link>
              </li>
              <li>
                <Link href='/about'>About</Link>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
};

export default Nav;
