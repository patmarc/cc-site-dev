import { useState, useContext } from 'react';
// import Play from './player/Play';
// import Pause from './player/Pause';
import { PlayerContext } from '../pages/browse';
import { useStore } from '../store/Store';
import { AiFillPlayCircle } from 'react-icons/ai';
import { AiFillPauseCircle } from 'react-icons/ai';
import playBtnStyles from '../styles/PlayBtn.module.css';

const PlayBtn = ({ id }) => {
  const { playing, setPlaying, idTrack, setIdTrack, setPlaybar, played, setPlayed } = useStore();

  const handlePlayPause = () => {
    setPlaying(!playing);
    setIdTrack(id);
    setPlaybar(true);
    //Selected track set its position to the beginning
    if (idTrack !== id) {
      setPlayed(0);
    }
  };

  return (
    <div id={`btn${id}`} className={playBtnStyles.btn} onClick={handlePlayPause}>
      {idTrack === id && playing ? (
        <span>
          <AiFillPauseCircle />
        </span>
      ) : (
        <AiFillPlayCircle />
      )}
    </div>
  );
};

export default PlayBtn;
