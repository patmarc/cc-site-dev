import { useStore } from '../store/Store';
import CheckBoxList from './filters/CheckBoxList';
import Accordion from './Accordion';
import DurationRangeSlider from './filters/DurationRangeSlider';
import TempoRangeSlider from './filters/TempoRangeSlider';
import SearchFilter from './filters/SearchFilter';
import { GiMusicSpell } from 'react-icons/gi';
import { MdFavorite } from 'react-icons/md';
import { RiPlayListAddLine } from 'react-icons/ri';
import { MdLibraryMusic } from 'react-icons/md';
import { BiTime } from 'react-icons/bi';
import { GiMetronome } from 'react-icons/gi';
import { BsPersonSquare } from 'react-icons/bs';
import { ImPriceTags } from 'react-icons/im';
import styles from '../styles/Sidebar.module.css';
import SelectTags from '@/components/filters/SelectTags';
import SelectAuthors from '@/components/filters/SelectAuthors';

const Sidebar = ({ genres, tags, authors }) => {
  const { home, setHome, setFavoriteView, setPlaylistView } = useStore();
  const tagNames = tags.map((tag) => tag.tag_name);

  const authorNames = authors.map((author) => author.author_name);

  const handleHomeView = () => {
    setFavoriteView(false);
    setPlaylistView(false);
  };
  const handleFavoriteView = () => {
    setFavoriteView(true);
    setPlaylistView(false);
  };
  const handlePlaylistView = () => {
    setPlaylistView(true);
    setFavoriteView(false);
  };

  return (
    <div className={styles.sidebar}>
      <div className='row ps-4'>
        <div className={styles.sidebarHeader}>Explore</div>
        <a className='mb-4' href='#' onClick={handleHomeView}>
          <GiMusicSpell />
          <span className='ps-3'>All Music</span>
        </a>
        <div className={styles.sidebarHeader}>Save</div>
        <a className='mb-4' href='#' onClick={handleFavoriteView}>
          <MdFavorite />
          <span className='ps-3'>Favorites</span>
        </a>
        <a className='mb-3' href='#' onClick={handlePlaylistView}>
          <RiPlayListAddLine />
          <span className='ps-3'>Playlists</span>
        </a>
        <div className={styles.sidebarHeader}>Filter by</div>
      </div>

      <Accordion
        title1={<MdLibraryMusic />}
        filter1={<CheckBoxList genres={genres} />}
        title2={<ImPriceTags />}
        filter2={<SelectTags values={tagNames} />}
        title3={<BsPersonSquare />}
        filter3={<SelectAuthors values={authorNames} />}
        title4={<BiTime />}
        filter4={
          <DurationRangeSlider
            min={0.0}
            max={10.0}
            defaultValue={[0.0, 10.0]}
            marks={{ 0.0: '0:00', 5.0: '5:00', 10.0: '10:00' }}
            tipFormatter={(value) => {
              return value.toString().replace('.', ':') + ' MIN';
            }}
            step={0.01}
          />
        }
        title5={<GiMetronome />}
        filter5={
          <TempoRangeSlider
            min={0}
            max={200}
            defaultValue={[60, 200]}
            marks={{ 0: 0, 100: 100, 200: 200 }}
            tipFormatter={(value) => `${value} BPM`}
          />
        }
      />
    </div>
  );
};

export default Sidebar;
