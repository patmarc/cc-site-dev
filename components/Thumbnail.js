import { useEffect } from 'react';
import Image from 'next/image';

const Thumbnail = () => {
  const getRandomNumber = (number) => {
    return Math.floor(Math.random() * number);
  };
  const getThumbnail = () => {
    const urlId = getRandomNumber(400);
    const url = `https://picsum.photos/id/${urlId}/48`;
    return url;
  };
  let thumbnail = '';

  useEffect(() => {
    thumbnail = getThumbnail();
    console.log(thumbnail);
  }, []);
  return (
    <div>
      <Image src={getThumbnail()} alt='music track picture' width={48} height={48} />
    </div>
  );
};

export default Thumbnail;
