import Link from 'next/link';
import dynamic from 'next/dynamic';
import trackStyles from '../styles/Track.module.css';
const PlayBtn = dynamic(() => import('./PlayBtn'));
// import PlayBtn from './PlayBtn';
import Favorite from './player/Favorite';
import AddPlaylist from './player/AddPlaylist';
import Share from './player/Share';
import Rating from '@/components/player/Rating';
import { useStore } from '../store/Store';
// import Thumbnail from '@/components/Thumbnail';
import Unsplash from 'react-unsplash-wrapper';

const TrackItem = ({ track }) => {
  const { playing, setPlaying, idTrack, setIdTrack, setPlaybar, played, setPlayed } = useStore();

  const handlePlayPause = () => {
    setPlaying(!playing);
    setIdTrack(track.track_id);
    setPlaybar(true);
    //Selected track set its position to the beginning
    if (idTrack !== track.track_id) {
      setPlayed(0);
    }
  };

  return (
    <div
      className={`${trackStyles.item} row ${
        idTrack === track.track_id && playing ? trackStyles.selected : ''
      }`}
      // onClick={handlePlayPause}
    >
      <div className={`flex col-1 align-self-center `}>
        {/* <Thumbnail /> */}
        <Unsplash width='48' height='48' keywords={track.title.split(' ').join(',')} img />
        <div style={{ position: 'relative', left: '-40px', top: '8px' }}>
          <PlayBtn id={track.track_id} />
        </div>
      </div>
      <div className={`col-2 align-self-center ${trackStyles.itemText}`}>
        <div className='fw-bold'>{track.title}</div>
        <div>{track.author_name}</div>
      </div>

      <div className={`col-1 align-self-center ${trackStyles.itemText}`}>
        {track.duration.toString().replace('.', ':')}
      </div>
      <div className={`col-1 align-self-center ${trackStyles.itemText}`}>{track.tempo}</div>
      <div className={`col-3 align-self-center ${trackStyles.tagList}`}>
        {track.tag_name.map((tag, index) => {
          return (
            index < 4 && (
              <span key={index} className={trackStyles.tag}>
                {tag}
              </span>
            )
          );
        })}
      </div>
      <div
        className={`${trackStyles.icons} col-2  d-flex justify-content-center align-self-center`}
      >
        <Favorite id={parseInt(track.track_id)} />
        <AddPlaylist />
        <Share id={track.track_id} title={track.title} />
        <Rating />
      </div>
      <div className='col-1 align-self-center'>
        <Link href='/tracks/[id]' as={`/tracks/${track.track_id}`}>
          <button type='button' className={`${trackStyles.details} btn btn-outline-dark `}>
            Details
          </button>
        </Link>
      </div>
      <div className='col-1 align-self-center'>
        <Link href='#' as={``}>
          <button onClick={'#'} className={`${trackStyles.download} btn btn-outline-dark `}>
            Download
          </button>
        </Link>
      </div>
    </div>
  );
};

export default TrackItem;
