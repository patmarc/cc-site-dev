import { useState, useEffect } from 'react';
import TrackItem from './TrackItem';
import { useStore } from '@/store/Store';
import { useSession } from 'next-auth/client';

export default function TrackList({ tracks }) {
  const {
    home,
    favoriteView,
    favorite,
    setFavorite,
    favoriteChanged,
    playlistView,
    cbCheck,
    selectedTags,
    selectedAuthors,
    durationValues,
    tempoValues,
    search,
  } = useStore();

  const [stateTracks, setStateTracks] = useState({});

  const [session] = useSession();

  // User is logged in
  // Fetch content from protected API route
  // Fetch data on loading page (session), and on favorite clicked (favoriteChanged)
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch('/api/favorites');
        const json = await res.json();
        let ids = [];
        json.map((fav) => {
          ids.push(fav.track_id);
        });
        setFavorite(ids);
      } catch (error) {
        console.log(error);
      }
    };
    if (session) {
      fetchData();
    }
  }, [session, favoriteChanged]);

  const filterSearch = (track) => {
    if (search.length > 0) {
      return (
        track.title.toLowerCase().includes(search.toLowerCase().trim()) ||
        track.author_name.toLowerCase().includes(search.toLowerCase().trim()) ||
        track.tag_name.some((tag) => tag.includes(search.toLowerCase().trim()))
      );
    } else {
      return track;
    }
  };

  const filterGenre = (track) => {
    if (cbCheck.length > 0) {
      return cbCheck.indexOf(track.genre_id) > -1;
    } else {
      return track;
    }
  };
  const filterTag = (track) => {
    if (selectedTags.length > 0) {
      const tagValues = selectedTags.map((tag) => {
        return tag.value;
      });
      return track.tag_name.some((tag) => tagValues.indexOf(tag) > -1);
    } else {
      return track;
    }
  };
  const filterAuthor = (track) => {
    if (selectedAuthors.length > 0) {
      const authorValues = selectedAuthors.map((author) => {
        return author.value;
      });
      return authorValues.includes(track.author_name);
    } else {
      return track;
    }
  };

  const filterDuration = (track) => {
    if (durationValues[0] >= 0.01 || durationValues[1] <= 9.99) {
      return track.duration >= durationValues[0] && track.duration <= durationValues[1];
    } else {
      return track;
    }
  };
  const filterTempo = (track) => {
    if (tempoValues[0] >= 60 && tempoValues[1] <= 200) {
      return track.tempo >= tempoValues[0] && track.tempo <= tempoValues[1];
    } else {
      return track;
    }
  };

  const filteredTracks = tracks
    .filter((track) => filterSearch(track))
    .filter((track) => filterGenre(track))
    .filter((track) => filterTag(track))
    .filter((track) => filterAuthor(track))
    .filter((track) => filterDuration(track))
    .filter((track) => filterTempo(track));

  //In favoriteView map checks for equality with == instead of === because the type is
  // string or number. It's definitely not the best solution, but that should be ok until
  // I have more time to dedicate to find where and how track_id int gets casted into a
  // string.
  return (
    <div>
      {favoriteView
        ? favorite.map((id) =>
            tracks.map(
              (track) => track.track_id == id && <TrackItem key={track.track_id} track={track} />
            )
          )
        : playlistView
        ? 'playlist'
        : filteredTracks.map((track) => <TrackItem key={track.track_id} track={track} />)}
    </div>
  );
}

// export const MemoizedTrackList = React.memo(TrackList);
