import { useState, useEffect } from 'react';
import { useStore } from '../../store/Store';
import styles from '../../styles/CheckBox.module.css';

const CheckBox = ({ genreId, genreName }) => {
  const [check, setCheck] = useState(false);
  const { cbCheck, setCBCheck } = useStore();

  const handleCheck = (event) => {
    setCheck(event.target.checked);
  };

  const addGenreId = () => {
    setCBCheck([...cbCheck, genreId]);
  };

  const removeGenreId = () => {
    const copyCBCheck = [...cbCheck];
    copyCBCheck.splice(cbCheck.indexOf(genreId), 1);
    setCBCheck(copyCBCheck);
  };

  // useEffect to prevent infinite loop
  useEffect(() => {
    check && addGenreId();
    if (check === false && cbCheck.indexOf(genreId) > -1) {
      removeGenreId();
    }
  }, [check]);

  return (
    <div className={`form-check ${styles.checkbox} `}>
      <input
        id={genreId}
        type='checkbox'
        className={`form-check-input ${styles.checkboxInput} `}
        checked={check}
        onChange={(e) => handleCheck(e)}
      />
      <label className={styles.checkboxLabel} htmlFor={genreId}>
        {genreName}
      </label>
    </div>
  );
};

export default CheckBox;
