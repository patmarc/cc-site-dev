import { server } from '../../config';
import CheckBox from './CheckBox';

export default function CheckBoxList({ genres }) {
  return (
    <div>
      {genres.map((genre) => {
        return <CheckBox key={genre.genre_id} genreId={genre.genre_id} genreName={genre.name} />;
      })}
    </div>
  );
}
