import React, { useState } from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { useStore } from '../../store/Store';
import styles from '../../styles/RangeSlider.module.css';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

export default function DurationRangeSlider({ min, max, defaultValue, marks, tipFormatter, step }) {
  const [value, setValue] = useState(defaultValue);
  const { durationValues, setDurationValues } = useStore();

  const handleChange = (value) => {
    setValue(value);
    setDurationValues(value);
  };

  return (
    <div className={styles.rangeSlider}>
      <Range
        min={min}
        max={max}
        defaultValue={defaultValue}
        value={value}
        onChange={handleChange}
        marks={marks}
        tipFormatter={tipFormatter}
        tipProps={{
          placement: 'bottom',
        }}
        step={step}
      />
    </div>
  );
}
