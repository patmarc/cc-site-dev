import styles from '../../styles/SearchFilter.module.css';

const SearchFilter = () => {
  const searchText = 'Type here to filter';
  return (
    <div className={styles.searchFilter}>
      <input className='' placeholder={searchText} type='text' className={''} />
      <div className={styles.tagList}>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
        <div>Tag about tracks</div>
      </div>
    </div>
  );
};

export default SearchFilter;
