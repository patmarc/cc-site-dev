import { useState, useEffect } from 'react';
import { useStore } from '@/store/Store';
import ReactSelect from 'react-select';
import makeAnimated from 'react-select/animated';

const SelectAuthors = ({ values }) => {
  const animatedComponents = makeAnimated();
  const { selectedAuthors, setSelectedAuthors } = useStore();
  const [selectedOption, setSelectedOption] = useState([]);
  const options = [];

  for (const item of values) {
    let obj = { value: item, label: item };
    options.push(obj);
  }

  const handleChange = (selectedOption) => {
    setSelectedOption(selectedOption);
  };
  useEffect(() => {
    setSelectedAuthors(selectedOption);
  });

  return (
    <div>
      <ReactSelect
        isMulti
        onChange={handleChange}
        options={options}
        components={animatedComponents}
      />
    </div>
  );
};

export default SelectAuthors;
