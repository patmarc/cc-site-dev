import { useState, useEffect } from 'react';
import { useStore } from '@/store/Store';
import ReactSelect from 'react-select';
import makeAnimated from 'react-select/animated';

const SelectTags = ({ values }) => {
  const animatedComponents = makeAnimated();
  const { selectedTags, setSelectedTags } = useStore();
  const [selectedOption, setSelectedOption] = useState([]);
  const options = [];

  for (const item of values) {
    let obj = { value: item.toLowerCase(), label: item.toLowerCase() };
    options.push(obj);
  }

  const handleChange = (selectedOption) => {
    setSelectedOption(selectedOption);
  };
  useEffect(() => {
    setSelectedTags(selectedOption);
  });

  return (
    <div>
      <ReactSelect
        isMulti
        onChange={handleChange}
        options={options}
        components={animatedComponents}
      />
    </div>
  );
};

export default SelectTags;
