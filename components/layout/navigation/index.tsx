import { tw } from 'twind';
import { useState, useEffect } from 'react';
import Button from '@/components/layout/button';
import Link from 'next/link';
import { IoAddCircle } from 'react-icons/io5';
import { useStore } from '@/store/Store';
import { signIn, signOut, useSession } from 'next-auth/client';

interface IMenuButton {
  toggleMenu: React.MouseEventHandler<HTMLButtonElement>;
  showMenu: boolean;
}

type Link = {
  label: string;
  href: string;
};

const links = [
  {
    label: `Home`,
    href: `/`,
  },
  {
    label: `Music`,
    href: `/browse`,
  },
  {
    label: `About`,
    href: `/about`,
  },
  {
    label: `Blog`,
    href: `/`,
  },
];

const secondaryLinks = [
  {
    label: `Log in`,
    href: `/signin`,
  },
  {
    label: `Sign up`,
    href: `/signup`,
  },
];

const MenuButton = ({ toggleMenu, showMenu }: IMenuButton) => (
  <button
    type='button'
    aria-controls='mobile-menu'
    aria-expanded={showMenu}
    onClick={toggleMenu}
    className={tw(`p-2 text-gray-400`)}
  >
    <span className={tw(`sr-only`)}>Open menu</span>
    {showMenu ? (
      <svg
        className={tw(`h-6 w-6`)}
        xmlns='http://www.w3.org/2000/svg'
        fill='none'
        viewBox='0 0 24 24'
        stroke='currentColor'
        aria-hidden='true'
        width={24}
        height={24}
      >
        <path
          strokeLinecap='round'
          strokeLinejoin='round'
          strokeWidth={2}
          d='M6 18L18 6M6 6l12 12'
        />
      </svg>
    ) : (
      <svg
        className={tw(`h-6 w-6`)}
        xmlns='http://www.w3.org/2000/svg'
        fill='none'
        viewBox='0 0 24 24'
        stroke='currentColor'
        aria-hidden='true'
        width={24}
        height={24}
      >
        <path
          strokeLinecap='round'
          strokeLinejoin='round'
          strokeWidth={2}
          d='M4 6h16M4 12h16M4 18h16'
        />
      </svg>
    )}
  </button>
);

const MobileMenu = () => (
  <div className={tw(`md:hidden`)}>
    <div className={tw(`px-2 pt-2 pb-3 space-y-1 sm:px-3`)}>
      {links.map((link: Link) => (
        <a
          href={link.href}
          className={tw(`text-gray-500 block px-3 py-2 text-base font-medium`)}
          key={link.label}
        >
          {link.label}
        </a>
      ))}
    </div>
    <div className={tw(`pt-4 pb-3 border-t border-gray-400`)}>
      <div className={tw(`px-2 space-y-1`)}>
        {secondaryLinks.map((link: Link) => (
          <a
            key={`mobile-${link.label}`}
            href={link.href}
            className={tw(`block px-3 py-2 text-base font-medium text-gray-500`)}
          >
            {link.label}
          </a>
        ))}
      </div>
    </div>
  </div>
);

const Navigation = () => {
  const [showMenu, setShowMenu] = useState(false);
  const toggleMenu = () => setShowMenu(!showMenu);
  const { setModalSignInOpen, setModalSignUpOpen } = useStore();

  const openModalSignIn = () => {
    setModalSignInOpen(true);
  };
  const openModalSignUp = () => {
    setModalSignUpOpen(true);
  };

  const [session] = useSession();

  return (
    <>
      <nav className={tw(`bg-white w-full fixed z-10`)}>
        <div className={tw(`px-4 sm:px-6 lg:px-8`)}>
          <div className={tw(`flex items-center justify-between h-24`)}>
            <div className={tw(`flex items-center`)}>
              <div className={tw(`flex-shrink-0`)}>
                <img className={tw(`h-12 w-12`)} src='logo.svg' alt='logo' width={48} height={48} />
              </div>
              <div className={tw(`hidden md:block`)}>
                <div className={tw(`ml-10 flex items-baseline space-x-4`)}>
                  {links.map((link: Link) => (
                    <a
                      key={link.label}
                      href={link.href}
                      className={tw(
                        `text-gray-500 hover:text-gray-600 px-3 py-2 rounded-md font-medium`
                      )}
                    >
                      {link.label}
                    </a>
                  ))}
                </div>
              </div>
            </div>
            <div className={tw(`hidden md:block`)}>
              <div className={tw(`ml-4 flex items-center md:ml-6`)}>
                <div className={tw('border-0 mr-10 flex')}>
                  <div className={tw(`self-center text-2xl text-purple-400`)}>
                    <IoAddCircle />
                  </div>
                  <Link href='/add-music'>
                    <span className={tw('cursor-pointer mr-0 hover:text-purple-500')}>
                      Add your music
                    </span>
                  </Link>
                </div>
                <Link href='/contact'>
                  <span className={tw('cursor-pointer mr-10 hover:text-purple-500')}>Contact</span>
                </Link>
                {!session && (
                  <>
                    <Link href='/signin'>
                      <span className={tw('cursor-pointer mr-10 hover:text-purple-500')}>
                        Log in{' '}
                      </span>
                    </Link>
                    <Link href='/signup'>
                      <button
                        className={tw(
                          ` font-sans font-medium py-2 px-4 border rounded bg-gradient-to-r from-purple-400 to-indigo-500 text-white border-indigo-500 hover:bg-indigo-600 uppercase`
                        )}
                      >
                        Sign Up
                      </button>
                    </Link>
                  </>
                )}
                {session && (
                  <>
                    {/* {session.user.id} */}
                    {/* {session.user.name} */}
                    {session.user.email}

                    <button
                      className={tw(
                        ` font-sans text-sm ml-10 py-2 px-4 border rounded bg-gradient-to-r from-purple-400 to-indigo-500 text-white border-indigo-500 hover:bg-indigo-600 uppercase`
                      )}
                      onClick={() => signOut()}
                    >
                      Log out
                    </button>
                  </>
                )}
              </div>
            </div>
            <div className={tw(`-mr-2 flex md:hidden`)}>
              <MenuButton showMenu={showMenu} toggleMenu={toggleMenu} />
            </div>
          </div>
        </div>
        {showMenu ? <MobileMenu /> : null}
      </nav>
    </>
  );
};

export default Navigation;
