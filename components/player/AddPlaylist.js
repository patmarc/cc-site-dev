import { RiPlayListAddLine } from 'react-icons/ri';

const AddPlaylist = () => {
  return (
    <div className='add-playlist mx-3'>
      <RiPlayListAddLine />
    </div>
  );
};

export default AddPlaylist;
