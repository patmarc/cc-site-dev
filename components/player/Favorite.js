import { useState, useEffect } from 'react';
import { MdFavoriteBorder } from 'react-icons/md';
import { MdFavorite } from 'react-icons/md';
import ReactModal from 'react-modal';
import { GrClose } from 'react-icons/gr';
import Button from '@/components/layout/button';
import styles from '@/styles/Modal.module.css';
import Link from 'next/link';

import { useStore } from '@/store/Store';
import { tw } from 'twind';

import { useSession } from 'next-auth/client';

ReactModal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.75)';

const Favorite = ({ id }) => {
  const { SetFavoriteChanged, favorite, setFavorite, msg, setMsg } = useStore();

  const [modalOpen, setModalOpen] = useState(false);
  const [session] = useSession();

  let idExists = false;

  ReactModal.setAppElement('body');

  const handleFavorite = async () => {
    if (!session && msg === false) {
      setModalOpen(true);
      setMsg(true);
    }
    if (session) {
      SetFavoriteChanged(true);

      if (favorite.includes(id)) {
        idExists = true;
      } else {
        idExists = false;
      }
      let obj = { idExists, id };
      //Sends idExists and id to back-end
      const res = await fetch('/api/addDelFavorite', {
        body: JSON.stringify(obj),
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'POST',
      });
      SetFavoriteChanged(false);
    } else {
      idExists = favorite.includes(id);

      //Add
      !idExists && setFavorite([...favorite, id]);

      //Remove
      idExists && setFavorite(favorite.filter((idFav) => idFav !== id));
    }
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  return (
    <>
      {console.log('favorite', favorite)}
      <div className='favorite mx-3'>
        <a href='#' onClick={() => handleFavorite()} title='favorite'>
          {favorite.length !== 0 && favorite.includes(id) ? (
            <span className={tw('text-purple-400')}>
              <MdFavorite />
            </span>
          ) : (
            <MdFavoriteBorder />
          )}
        </a>
      </div>
      <ReactModal
        isOpen={modalOpen}
        onRequestClose={closeModal}
        closeTimeoutMS={500}
        className='Modal'
      >
        <div className={styles.container}>
          <button onClick={closeModal} className={styles.cross}>
            <GrClose />
          </button>
          <div>
            <div>
              <div className={tw('text-lg text-red-600')}>
                Saving favorites is not permanente for non register users. You can mark your
                favorites and see them on the favorites menu, but on page refresh or on page change
                the data is lost. Sign up for free and UNLOCK WITH YOUR FREE ACCOUNT permanent
                saving for Add Favorites, Add Ratings, Create Playlists.
              </div>
              {/* <img className={tw('mt-10')} src='images/submit-error.svg' alt='' /> */}
            </div>
          </div>

          <div className={styles.button}>
            <Link href='/signup'>
              <button
                className={tw(
                  ` font-sans font-medium mb-4 py-2 px-4 border rounded bg-gradient-to-r
                  from-purple-400 to-indigo-500 text-white border-indigo-500
                  hover:bg-indigo-600 uppercase`
                )}
              >
                Sign Up
              </button>
            </Link>
            <Button variant='' onClick={closeModal}>
              close
            </Button>
          </div>
        </div>
      </ReactModal>
    </>
  );
};

export default Favorite;
