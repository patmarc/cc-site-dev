import { useStore } from '../../store/Store';

const Info = () => {
  const { title } = useStore();
  return (
    <>
      <div className='col-2 align-self-center'>
        <div className='fw-bold fs-6'>{title} </div>
        <div className='fs-6'>Author's name</div>
      </div>
    </>
  );
};

export default Info;
