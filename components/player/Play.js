const Play = () => {
  return (
    <svg className='button' viewBox='0 0 60 60'>
      <polygon points='0,0 50,30 0,60' />
    </svg>
  );
};

export default Play;
