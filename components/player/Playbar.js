import { useState, useEffect } from 'react';
import styles from '@/styles/Playbar.module.css';
import PlayBtnPlayer from '@/components/player/PlayBtnPlayer';
import { useStore } from '../../store/Store';
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import Favorite from './Favorite';
import AddPlaylist from './AddPlaylist';
import Share from './Share';
import Info from './Info';
import { IoVolumeHighOutline } from 'react-icons/io5';
import dynamic from 'next/dynamic';
// importing external modules depending on window:
// https://nextjs.org/docs/advanced-features/dynamic-import#with-no-ssr
const ReactWaves = dynamic(() => import('@dschoon/react-waves'), { ssr: false });

const Playbar = () => {
  const {
    volume,
    setVolume,
    played,
    setPlayed,
    seeking,
    setSeeking,
    playing,
    setPlaying,
    playbar,
    idTrack,
    url,
    prevUrl,
    setPrevUrl,
    title,
    refPlayer,
  } = useStore();

  const handleVolumeChange = (value) => {
    setVolume(value);
  };

  const handleSeekChange = (value) => {
    setPlayed(value);
  };
  const handleSeekMouseDown = () => {
    setSeeking(true);
  };
  const handleSeekMouseUp = () => {
    setSeeking(false);
    // refPlayer.current.seekTo(parseFloat(e.target.value));
    refPlayer.current.seekTo(played);
  };

  return (
    <div className={`${styles.playbar} container-fluid ${playbar ? 'd-block' : 'd-none'}`}>
      <div className='row'>
        <Info />
        <div className='seek col-5 align-self-center'>
          <Slider
            step={0.01}
            tooltip={false}
            min={0}
            max={0.999999}
            value={played}
            onChangeStart={handleSeekMouseDown}
            onChange={handleSeekChange}
            onChangeComplete={handleSeekMouseUp}
          />
        </div>
        <div className='col-2 d-flex justify-content-center'>
          <PlayBtnPlayer />
        </div>
        <div className='volume col-1 d-flex justify-content-around align-self-center'>
          <div className={styles.volumeIcon}>
            <IoVolumeHighOutline />
          </div>
          <Slider
            step={0.01}
            tooltip={false}
            max={1}
            min={0}
            value={volume}
            onChange={handleVolumeChange}
          />
        </div>
        <div className='col-2 d-flex justify-content-center align-self-center'>
          <Favorite />
          <AddPlaylist />
          <Share id={idTrack} title={title} />
        </div>
      </div>
    </div>
  );
};

export default Playbar;
