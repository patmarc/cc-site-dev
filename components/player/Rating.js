import { AiOutlineStar } from 'react-icons/ai';
import { useStore } from '@/store/Store';

const Rating = () => {
  const { modalRatingOpen, setModalRatingOpen } = useStore();

  const openModalRating = () => {
    setModalRatingOpen(!modalRatingOpen);
    console.log('modalRating:', modalRatingOpen);
  };

  return (
    <button className='mx-3' onClick={openModalRating} aria-label='rating'>
      <AiOutlineStar />
    </button>
  );
};

export default Rating;
