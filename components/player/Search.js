import { useState } from 'react';
import searchStyles from '../../styles/Search.module.css';
import { useStore } from '@/store/Store';

const Search = () => {
  const searchText = 'Search by title, artist, tags';
  const { search, setSearch } = useStore();

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  return (
    <input
      value={search}
      onChange={handleSearch}
      placeholder={searchText}
      type='text'
      className={searchStyles.inputField}
    />
  );
};

export default Search;
