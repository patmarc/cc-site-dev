import { FiShare2 } from 'react-icons/fi';
import { useStore } from '@/store/Store';

const Share = ({ id, title }) => {
  const { setModalSocialOpen, setModalSocialData } = useStore();

  const openModalSocial = () => {
    setModalSocialOpen(true);
    setModalSocialData([id, title]);
  };

  return (
    <>
      <button onClick={openModalSocial} className='Share mx-3' aria-label='share'>
        <FiShare2 />
      </button>
    </>
  );
};

export default Share;
