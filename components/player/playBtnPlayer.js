import { useState, useContext } from 'react';
import { PlayerContext } from '@/pages/browse';
import { useStore } from '@/store/Store';
import { IoPlay } from 'react-icons/io5';
import { IoPause } from 'react-icons/io5';
import playBtnPlayerStyles from '@/styles/PlayBtnPlayer.module.css';

const PlayBtnPlayer = () => {
  const { playing, setPlaying } = useStore();

  const handlePlayPause = () => {
    setPlaying(!playing);
  };

  return (
    <div id='play-btn-player' className={playBtnPlayerStyles.btn} onClick={handlePlayPause}>
      {playing ? <IoPause /> : <IoPlay />}
    </div>
  );
};

export default PlayBtnPlayer;
