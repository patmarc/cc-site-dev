import Layout from '@/components/Layout';
import '@/styles/globals.css';
import '@/styles/slider.css';
import '@fontsource/inter';
import { StoreProvider } from '@/store/Store';

import { AppProps } from 'next/app';
// import { NextWebVitalsMetric } from 'next/app';
import '@fontsource/inter';

import { setup } from 'twind';
import twindConfig from '../twind.config';

import { Provider as NextAuthProvider } from 'next-auth/client';

if (typeof window !== `undefined`) {
  setup(twindConfig);
}

function MyApp({ Component, pageProps }) {
  return (
    <NextAuthProvider session={pageProps.session}>
      <Layout>
        <StoreProvider>
          <Component {...pageProps} />
        </StoreProvider>
      </Layout>
    </NextAuthProvider>
  );
}

// export function reportWebVitals(metric: NextWebVitalsMetric) {
//   console.log(metric)
// }

export default MyApp;
