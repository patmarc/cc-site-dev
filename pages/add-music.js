import { useState, useRef } from 'react';
import Meta from '@/components/Meta';
import { tw } from 'twind';
import Footer from '@/components/layout/footer';
import Unsplash from 'react-unsplash-wrapper';
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import ReactModal from 'react-modal';
import styles from '@/styles/AddMusic.module.css';
import { GrClose } from 'react-icons/gr';
import Button from '@/components/layout/button';
import Recaptcha from 'react-google-recaptcha';

ReactModal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.75)';

const AddMusic = () => {
  const [name, setName] = useState('');
  const [contact, setContact] = useState('');
  const [email, setEmail] = useState('');
  const [title, setTitle] = useState('');
  const [link, setLink] = useState('');
  const [description, setDescription] = useState('');
  const [genre, setGenre] = useState('');
  const [tags, setTags] = useState('');
  const [minutes, setMinutes] = useState('');
  const [seconds, setSeconds] = useState('');
  let duration = `${minutes}.${seconds}`;
  const [tempoSlider, setTempoSlider] = useState(120);
  const [submitted, setSubmitted] = useState(false);
  const [message, setMessage] = useState('');
  const [modalOpen, setModalOpen] = useState(false);
  const reRef = useRef();

  ReactModal.setAppElement('body');

  const closeModal = () => {
    setModalOpen(false);
  };
  
  const handleSubmit = async (e) => {
    e.preventDefault();

    const tokenRecaptcha = await reRef.current.executeAsync();
    reRef.current.reset();

    let data = {
      name,
      email,
      title,
      link,
      description,
      genre,
      tags,
      duration,
      tempoSlider,
      tokenRecaptcha,
    };
    fetch('/api/addMusic', {
      method: 'POST',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }).then((res) => {
      setModalOpen(true);
      if (res.status === 200) {
        setMessage('Thank you for your submission!');
        setSubmitted(true);
        setName('');
        setContact('');
        setEmail('');
        setTitle('');
        setLink('');
        setDescription('');
        setGenre('');
        setTags('');
        setMinutes('');
        setSeconds('');
      } else {
        setMessage('Oops something went wrong! Your submission was not sent');
      }
    });
  };

  const handleTempoSlider = (value) => {
    setTempoSlider(value);
  };

  const formatTempo = (value) => Math.round(value) + ' BPM';

  return (
    <>
      <Recaptcha
        sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
        size='invisible'
        ref={reRef}
      />
      <Meta
        title={'Add Your Music'}
        keywords={
          'free music for podcasts, free music youtube, free music library, free music for videos, free background music'
        }
        description={'Free Background Music For Your videos, Podcasts, Games, Applications.'}
      />
      <div className={tw('w-full flex flex-wrap pt-24 lg:pt-0 ')}>
        {/* image section */}
        <div className={tw('w-1/2 h-full hidden md:block')}>
          <Unsplash photoId='MuIvHRJbjA8' width='900' height='1100'>
            {/* <h1
              style={{
                color: '#a78bfa',
                fontFamily: 'helvetica',
                fontWeight: 'bold',
                textShadow: '1px 1px 2px black',
                textTransform: 'uppercase',
                fontSize: '100%',
                position: 'relative',
                bottom: '12%',
              }}
            >
              Share your music with the world!
            </h1>
            <p
              style={{
                color: '#fff',
                fontSize: '1.5rem',
                fontFamily: 'helvetica',
                textShadow: '1px 1px 2px black',
                position: 'relative',
                bottom: '12%',
                wordWrap: 'break-word',
              }}
            >
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </p> */}
          </Unsplash>
          {/* <img
            className='object-cover w-full h-screen hidden md:block'
            src='https://images.unsplash.com/photo-1592191136579-27d0d2d44e86?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=80'
          /> */}
        </div>
        <div className={tw('w-full md:w-1/2 flex flex-col')}>
          <div
            className={tw(
              'flex flex-col justify-center md:justify-start sm:pt-28 sm:px-24 bg-gray-50 h-full'
            )}
          >
            <div className={tw('text-center mb-3')}>
              <h1 className={tw('font-bold text-3xl text-gray-900 uppercase')}>Add your music</h1>
            </div>
            <form method='post'>
              <div className={tw('text-base md:text-lg mb-3 md:ml-1')}>
                <strong>Important!</strong> Your music/song must be published under creative commons
                or public domain licenses. The following creative commons licenses are accepted:
                Attribution CC BY, Attribution-ShareAlike CC BY-SA, and Attribution-NoDerivs CC
                BY-ND. <br />
                Please be aware that not all the submissions are guaranteed to be published. Each
                submitted music/song are curated to meet a minimum standard quality.
              </div>

              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Artist Name
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='text'
                      id='name'
                      name='name'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder='Max Martin'
                      value={name}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Email
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='email'
                      id='email'
                      name='email'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder='maxmartin@gmail.com'
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Place Where Fans Can Reach You
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='text'
                      id='contact'
                      name='contact'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder='https://twitter.com/maxmartin'
                      value={contact}
                      onChange={(e) => {
                        setContact(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex')}>
                <div className={tw('w-full px-1 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Music/Song Title
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    ></div>
                    <input
                      type='text'
                      id='title'
                      name='title'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder='Shake It Off'
                      value={title}
                      onChange={(e) => {
                        setTitle(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex')}>
                <div className={tw('w-full px-1 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    A link To Your Music/Song
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    ></div>
                    <input
                      type='text'
                      id='link'
                      name='link'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder=''
                      value={link}
                      onChange={(e) => {
                        setLink(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex')}>
                <div className={tw('w-full px-1 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Short Description
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    ></div>
                    <textarea
                      rows='3'
                      id='description'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-2 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      value={description}
                      onChange={(e) => {
                        setDescription(e.target.value);
                      }}
                      required
                      placeholder='An uptempo dance-pop song that incorporates a saxophone line and 
                      a looping drum beat, a handclap-based bridge, and synthesized saxophones.'
                    ></textarea>
                  </div>
                </div>
              </div>

              <div className={tw('flex ')}>
                <div className={tw('w-full px-1 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Genre
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    ></div>
                    <input
                      type='text'
                      id='genre'
                      name='genre'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder='uptempo dance-pop '
                      value={genre}
                      onChange={(e) => {
                        setGenre(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex')}>
                <div className={tw('w-full px-1 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Tags
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    ></div>
                    <input
                      type='text'
                      id='tags'
                      name='tags'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder='upbeat, happy, driving, joyful'
                      value={tags}
                      onChange={(e) => {
                        setTags(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex mb-8')}>
                <div className={tw('w-full px-1 mb-0')}>
                  <label htmlFor='' className='text-sm font-semibold px-1 mb-3 '>
                    Duration
                  </label>
                  <div className={tw('flex flex-row')}>
                    <div className={tw('w-20 mx-1')}>
                      <label htmlFor='' className={tw('text-xs text-gray-500 font-semibold')}>
                        Minutes
                      </label>
                      <div className={tw('')}>
                        <input
                          id='duration-minutes'
                          type='number'
                          min='0'
                          max='10'
                          className={tw(
                            'w-full text-lg py-2 px-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                          )}
                          value={minutes}
                          onChange={(e) => {
                            setMinutes(e.target.value);
                          }}
                          required
                        />
                      </div>
                    </div>
                    <div className={tw('w-20 mx-2')}>
                      <label htmlFor='' className={tw('text-xs text-gray-500 font-semibold')}>
                        Seconds
                      </label>
                      <div
                        className={tw(
                          'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                        )}
                      ></div>
                      <input
                        id='duration-seconds'
                        type='number'
                        min='0'
                        max='59'
                        className={tw(
                          'w-full text-lg py-2 px-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                        )}
                        value={seconds}
                        onChange={(e) => {
                          setSeconds(e.target.value);
                        }}
                        required
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className={tw('flex')}>
                <div className={tw('w-full px-1 mb-3')}>
                  <label htmlFor='' className='text-sm font-semibold px-1'>
                    Tempo ({formatTempo(tempoSlider)})
                  </label>
                  <Slider
                    step={0.01}
                    min={0}
                    max={200}
                    tooltip={false}
                    value={tempoSlider}
                    onChange={handleTempoSlider}
                  />
                </div>
              </div>
              <div className={tw('mb-3 ml-2 text-sm')}>
                {/* <a href='#'>Forgot your password?</a> */}
              </div>
              <div className={tw('flex')}>
                <div className={tw('w-full px-1 mb-5')}>
                  <button
                    type='submit'
                    className={tw(
                      'block w-full max-w-xs mx-auto bg-gradient-to-r from-purple-500 to-indigo-500 text-white border-indigo-500 hover:bg-indigo-600 rounded-lg px-3 py-3 font-semibold uppercase'
                    )}
                    onClick={(e) => {
                      handleSubmit(e);
                    }}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className={tw('mt-1')}>
        <Footer />
      </div>
      <ReactModal
        isOpen={modalOpen}
        onRequestClose={closeModal}
        closeTimeoutMS={500}
        className='Modal'
      >
        <div className={styles.container}>
          <button onClick={closeModal} className={styles.cross}>
            <GrClose />
          </button>
          <div>
            {submitted ? (
              <div>
                <div className={tw('text-purple-600')}>{message}</div>
                <img className={tw('mt-10')} src='images/thank-you.svg' alt='' />
              </div>
            ) : (
              <div>
                <div className={tw('text-red-600')}>{message}</div>
                <img className={tw('mt-10')} src='images/submit-error.svg' alt='' />
              </div>
            )}
          </div>
          <div className={styles.button}>
            <Button variant='uppercase' onClick={closeModal} primary>
              close
            </Button>
          </div>
        </div>
      </ReactModal>
    </>
  );
};

export default AddMusic;
