const { Pool } = require('pg');
const pool = new Pool();
import { getSession } from 'next-auth/client';

export default async (req, res) => {
  if (req.method !== 'POST') {
    res.status(400).send({ message: 'Request not allowed' });
    return;
  }

  const session = await getSession({ req });
  //The user id is defined in callbacks from file api/auth/[...nextauth.js]
  if (session) {
    // Gets idExists from front-end
    let idExists = JSON.parse(req.body.idExists);
    let trackID = JSON.parse(req.body.id);
    let favoriteQuery = null;
    console.log('trackid server', trackID);
    try {
      // If trackID is found, delete it from DB
      if (idExists) {
        favoriteQuery = await pool.query(
          'DELETE FROM favorite WHERE user_id = $1 AND track_id = $2;',
          [session.user.id, trackID]
        );
      } else {
        // If trackID is not found, insert it into the DB
        favoriteQuery = await pool.query(
          'INSERT INTO favorite (user_id, track_id) values ($1, $2);',
          [session.user.id, trackID]
        );
      }
      res.status(200).json(favoriteQuery.rows[0]);
    } catch (error) {
      console.log(error);
    }
  } else {
    // Not allowed
    res.status(401);
  }
  res.end();
};
