export default async function (req, res) {
  require('dotenv').config();
  const { name, email, title, link, description, genre, tags, duration, tempoSlider } = req.body;

  //-------- EMAIL ADDRESS VALIDATION -----------------------------------------------------------------

  async function checkEmail() {
    const response = await fetch(`https://api.testmail.top/domain/check?data=${email}`, {
      method: 'GET',
      headers: {
        Authorization: process.env.TESTMAIL_API_KEY,
      },
    });
    const data = await response.json();
    return data;
  }
  const { result } = await checkEmail();
  if (!result) {
    res.status(400);
    // console.log('result', result);
    return;
  }

  //-------- RECAPTCHA VALIDATION -----------------------------------------------------------------

  async function validateHuman(token) {
    const secretKey = process.env.RECAPTCHA_SECRET_KEY;
    const response = await fetch(
      `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${token}`,
      {
        method: 'POST',
      }
    );
    const data = await response.json();
    return data.success;
  }

  const human = await validateHuman(req.body.tokenRecaptcha);
  if (!human) {
    res.status(400);
    return;
  }

  //------------------------------------------------------------------------------------------------

  let nodemailer = require('nodemailer');

  const transporter = nodemailer.createTransport({
    port: 465,
    host: 'smtp.gmail.com',
    auth: {
      user: process.env.EMAIL_SMTP_SENDER,
      pass: process.env.EMAIL_SMTP_PASSWORD,
    },
    secure: true,
  });
  const mailData = {
    from: process.env.EMAIL_SMTP_SENDER,
    to: process.env.EMAIL_SMTP_RECEIVER,
    subject: `Message From ${email}`,
    // text: name + ' | Sent from: ' + email,
    html: `<div> <ul>
    <li><strong>Artist Name: </strong>${name} </li>
    <li><strong>Email: </strong>${email} </li>
    <li><strong>Title: </strong>${title} </li>
    <li><strong>Link: </strong>${link} </li>
    <li><strong>Description: </strong>${description} </li>
    <li><strong>Genre: </strong>${genre} </li>
    <li><strong>Tags: </strong>${tags} </li>
    <li><strong>Duration: </strong>${duration} </li>
    <li><strong>Tempo: </strong>${Math.round(tempoSlider)} </li>
    </ul>
    </div>`,
  };
  transporter.sendMail(mailData, function (err, info) {
    if (err) console.log(err);
    else console.log(info);
  });
  console.log('req.body: ', req.body);
  res.send('success');
}
