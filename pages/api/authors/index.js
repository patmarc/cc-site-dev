const { Pool } = require('pg');
const pool = new Pool();

export default async (req, res) => {
  try {
    const authors = await pool.query(
      'SELECT DISTINCT name AS author_name FROM author ORDER BY name ASC;'
    );
    res.status(200).json(authors.rows);
  } catch (error) {
    console.log(error);
  }
};
