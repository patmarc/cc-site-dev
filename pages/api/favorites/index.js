const { Pool } = require('pg');
const pool = new Pool();
import { getSession } from 'next-auth/client';

export default async (req, res) => {
  const session = await getSession({ req });
  //The user id is defined in callbacks from file api/auth/[...nextauth.js]
  if (session) {
    // Signed in
    try {
      const favorites = await pool.query('SELECT track_id FROM favorite WHERE user_id = $1;', [
        session.user.id,
      ]);
      // const favorites = await pool.query('SELECT track_id FROM favorite WHERE user_id = $1;', [
      //   req.query.id,
      // ]);

      res.status(200).json(favorites.rows);
    } catch (error) {
      console.log(error);
    }
  } else {
    // Not allowed
    res.status(401);
  }
  res.end();
};
