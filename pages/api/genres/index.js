const { Pool } = require('pg');
const pool = new Pool();

export default async (req, res) => {
  try {
    const genres = await pool.query('SELECT * FROM genre;');
    // console.log('db call');
    res.status(200).json(genres.rows);
  } catch (error) {
    console.log(error);
  }
};
