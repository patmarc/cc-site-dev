const { Pool } = require('pg');
const pool = new Pool();

export default async (req, res) => {
  try {
    const tags = await pool.query(
      'SELECT DISTINCT UNNEST(name) AS tag_name FROM tag ORDER BY tag_name ASC;'
    );

    res.status(200).json(tags.rows);
  } catch (error) {
    console.log(error);
  }
};
