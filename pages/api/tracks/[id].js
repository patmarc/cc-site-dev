const { Pool } = require('pg');
const pool = new Pool();

export default async (req, res) => {
  try {
    const track = await pool.query(
      'SELECT t.track_id,t.audio_url,t.title,t.description,t.genre_id,t.duration_id,t.tempo_id,a.author_id,a.name as author_name,d.duration_id,d.time as duration,te.tempo_id,te.value as tempo,tg.tag_id,tg.name as tag_name, ge.name as genre_name FROM track t INNER JOIN author a on t.author_id = a.author_id INNER JOIN tag tg on t.tag_id = tg.tag_id INNER JOIN duration d on t.duration_id = d.duration_id INNER JOIN tempo te on t.tempo_id = te.tempo_id INNER JOIN genre ge on t.genre_id = ge.genre_id WHERE track_id = $1;',
      [req.query.id]
    );
    res.status(200).json(track.rows[0]);
  } catch (error) {
    console.log(error);
  }
};

// 'SELECT * FROM track WHERE track_id = $1'
