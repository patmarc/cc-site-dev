require('dotenv').config();
const { Pool } = require('pg');
const pool = new Pool();

export default async (req, res) => {
  try {
    const tracks = await pool.query(
      // 'SELECT t.track_id,t.audio_url,t.title,t.description,t.genre_id,tg.tag_id,tg.name as tag_name FROM track t INNER JOIN tag tg on t.tag_id = tg.tag_id;'
      // 'SELECT t.track_id,t.audio_url,t.title,t.description,t.genre_id,t.duration_id,t.tempo_id,d.duration_id,d.time as duration,te.tempo_id,te.value as tempo,tg.tag_id,tg.name as tag_name FROM track t INNER JOIN tag tg on t.tag_id = tg.tag_id INNER JOIN duration d on t.duration_id = d.duration_id INNER JOIN tempo te on t.tempo_id = te.tempo_id;'
      'SELECT t.track_id,t.audio_url,t.title,t.description,t.genre_id,t.duration_id,t.tempo_id,a.author_id,a.name as author_name,d.duration_id,d.time as duration,te.tempo_id,te.value as tempo,tg.tag_id,tg.name as tag_name FROM track t INNER JOIN author a on t.author_id = a.author_id INNER JOIN tag tg on t.tag_id = tg.tag_id INNER JOIN duration d on t.duration_id = d.duration_id INNER JOIN tempo te on t.tempo_id = te.tempo_id;'
    );
    // console.log('db call');
    res.status(200).json(tracks.rows);
  } catch (error) {
    console.log(error);
  }
};

// ('SELECT t.track_id, t.audio_url, t.title, t.description, g.name AS genre FROM track t JOIN track_genre tg ON t.track_id = tg.track_id JOIN genre g ON g.genre_id = tg.genre_id');
