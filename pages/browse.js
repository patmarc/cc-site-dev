import { useEffect, createContext } from 'react';
import dynamic from 'next/dynamic';
import { server } from '../config';
import Meta from '@/components/Meta';
// import TrackList from '../components/TrackList';
const TrackList = dynamic(() => import('@/components/TrackList'));
import ReactPlayer from 'react-player/lazy';
import trackStyles from '../styles/Track.module.css';
// import Sidebar from '../components/Sidebar';
const Sidebar = dynamic(() => import('@/components/Sidebar'));
import Playbar from '../components/player/Playbar';
import { useStore } from '../store/Store';
import Search from '../components/player/Search';
import searchStyles from '../styles/Search.module.css';
import { AiOutlineSearch } from 'react-icons/ai';
const ModalSocial = dynamic(() => import('@/components/ModalSocial'));
// import ModalSocial from '@/components/ModalSocial';
const ModalRating = dynamic(() => import('@/components/ModalRating'));
// import ModalRating from '@/components/ModalRating';
import { tw } from 'twind';

export const PlayerContext = createContext();

export default function Browse({ tracks, genres, tags, authors }) {
  const {
    playing,
    setPlaying,
    idTrack,
    setIdTrack,
    volume,
    setVolume,
    played,
    url,
    setUrl,
    title,
    setTitle,
    setPlayed,
    seeking,
    setSeeking,
    loaded,
    refPlayer,
  } = useStore();

  const audioUrls = tracks.map((track) => track.audio_url);

  const titles = tracks.map((track) => track.title);

  useEffect(() => {
    setUrl(audioUrls[idTrack - 1]);
    setTitle(titles[idTrack - 1]);
  }, [audioUrls, titles, idTrack]);

  const handlePlayPause = () => {
    setPlaying(!playing);
  };
  const handlePlay = () => {
    setPlaying(true);
  };
  const handlePause = () => {
    setPlaying(false);
  };
  const handleStop = () => {
    setPlaying(false);
    setUrl(null);
  };
  const load = (url) => {
    setUrl(url);
  };
  const handleProgress = () => {
    const playedSeconds = refPlayer.current.getCurrentTime();
    const duration = refPlayer.current.getDuration();
    !seeking && setPlayed(playedSeconds / duration);
    // console.log('onProgress', played);
  };

  return (
    <>
      <Meta
        title={'Tons Of Free Background Music To Use In Your Projects'}
        keywords={
          'free music for podcasts, free music youtube, free music library, free music for videos, free background music'
        }
        description={
          'Browse an entire free music library comprised of thousands of music selections. Easily search and filter an expansive catalog to find the perfect music for your video, podcast, games projects.'
        }
      />
      <ReactPlayer
        ref={refPlayer}
        // url={track.audio.url}
        url={url}
        playing={playing}
        controls={false}
        volume={volume}
        onPlay={handlePlay}
        onPause={handlePause}
        width='0'
        height='0'
        onProgress={handleProgress}
        onSeek={console.log('')}
      />

      <ModalSocial />
      <ModalRating />

      <div className={tw('flex md:flex-row-reverse flex-wrap')}>
        <div className={tw('w-full md:w-56 fixed bottom-0 md:left-0 h-16 md:h-screen')}>
          <div className={tw('md:relative mt-4')}>
            <Sidebar genres={genres} tags={tags} authors={authors} />
          </div>
        </div>

        <div className={tw('w-full md:w-11/12 pl-10')}>
          <div id='main-search' className={tw('w-full fixed mt-16')}>
            {/* <span className={`${searchStyles.magnifier}`}>
              <AiOutlineSearch />
            </span> */}
            <div className=''>
              <Search className={searchStyles.inputField} />
            </div>
          </div>
          <div id='tracklist' className={tw('mt-32 ml-6')}>
            <div className={`${trackStyles.header} row font-weight-bold py-3`}>
              <div className='col-1'></div>
              <div className='col-2'>Title</div>
              <div className='col-1'>Duration</div>
              <div className='col-1'>Tempo</div>
              <div className='col-3'>Tags</div>
              <div className='col-2'></div>
              <div className='col-1'></div>
              <div className='col-1'></div>
            </div>
            <TrackList tracks={tracks} />
          </div>
        </div>

        <div className={tw('flex md:flex-row-reverse flex-wrap')}>
          <div className='row'>
            <Playbar />
          </div>
        </div>
      </div>
    </>
  );
}

export const getStaticProps = async () => {
  const resTracks = await fetch(`${server}/api/tracks`);
  const tracks = await resTracks.json();

  const resGenres = await fetch(`${server}/api/genres`);
  const genres = await resGenres.json();

  const resTags = await fetch(`${server}/api/tags`);
  const tags = await resTags.json();

  const resAuthors = await fetch(`${server}/api/authors`);
  const authors = await resAuthors.json();

  return {
    props: {
      tracks,
      genres,
      tags,
      authors,
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 60 minutes
    revalidate: 3600, // In seconds (60 minutes)
  };
};
