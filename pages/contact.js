import { useState, useRef } from 'react';
import Meta from '@/components/Meta';
import { tw } from 'twind';
import Footer from '@/components/layout/footer';
import ReactModal from 'react-modal';
import { GrClose } from 'react-icons/gr';
import Button from '@/components/layout/button';
import Recaptcha from 'react-google-recaptcha';
import styles from '@/styles/Contact.module.css';

ReactModal.defaultStyles.overlay.backgroundColor = 'rgba(0, 0, 0, 0.75)';

const contact = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');
  const [messageModal, setMessageModal] = useState('');
  const [submitted, setSubmitted] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const reRef = useRef();

  ReactModal.setAppElement('body');

  const closeModal = () => {
    setModalOpen(false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const tokenRecaptcha = await reRef.current.executeAsync();
    reRef.current.reset();

    let data = {
      name,
      email,
      subject,
      message,
      tokenRecaptcha,
    };
    fetch('/api/contact', {
      method: 'POST',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }).then((res) => {
      setModalOpen(true);
      if (res.status === 200) {
        setMessageModal('Thank you for your message!');
        setSubmitted(true);
        setName('');
        setEmail('');
        setSubject('');
        setMessage('');
      } else {
        setMessageModal('Oops something went wrong! Your message was not sent');
      }
    });
  };

  return (
    <>
      <Recaptcha
        sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
        size='invisible'
        ref={reRef}
      />
      <Meta
        title={'Contact'}
        keywords={
          'free music for podcasts, free music youtube, free music library, free music for videos, free background music'
        }
        description={'Free Background Music For Your videos, Podcasts, Games, Applications.'}
      />
      <div className={tw('w-full flex flex-wrap pt-24 lg:pt-0 ')}>
        {/* image section */}
        <div className={tw('w-1/2 h-screen hidden md:block')}>
          <img className='object-cover w-full h-screen hidden md:block' src='/images/contact.jpg' />
        </div>
        <div className={tw('w-full md:w-1/2 flex flex-col')}>
          <div
            className={tw(
              'flex flex-col justify-center md:justify-start pt-60 md:pt-24 md:px-24 bg-gray-50'
            )}
          >
            <div className={tw('text-center mb-3')}>
              <h1 className={tw('font-bold text-3xl text-gray-900 uppercase pt-1 pb-2')}>
                Contact
              </h1>
              {/* <div className={tw('text-center py-2 text-lg')}>
                <p className={tw('text-indigo-600 uppercase font-bold')}>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                </p>
                <div className={tw('text-gray-700 pb-2 md:pb-0')}>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta, soluta in
                  accusantium ut ratione quasi velit modi.
                </div>
              </div> */}
            </div>
            <form method='post' action='/api/auth/signin/email'>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className={tw('text-xs font-semibold px-1')}>
                    Name (optional)
                  </label>
                  <div className='flex'>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-account-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='text'
                      id='name'
                      name='name'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      value={name}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-xs font-semibold px-1'>
                    Email
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='email'
                      id='email'
                      name='email'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-xs font-semibold px-1'>
                    Subject (optional)
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='text'
                      id='subject'
                      name='subject'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      value={subject}
                      onChange={(e) => {
                        setSubject(e.target.value);
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-xs font-semibold px-1'>
                    Message
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <textarea
                      rows='5'
                      id='message'
                      className={tw(
                        'w-full -ml-10 pl-3 pr-2 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      value={message}
                      onChange={(e) => {
                        setMessage(e.target.value);
                      }}
                      required
                    ></textarea>
                  </div>
                </div>
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 py-4 mb-20')}>
                  <button
                    type='submit'
                    className={tw(
                      'block w-full max-w-xs mx-auto bg-gradient-to-r from-purple-500 to-indigo-500 text-white border-indigo-500 hover:bg-indigo-600 rounded-lg px-3 py-3 font-semibold uppercase'
                    )}
                    onClick={(e) => {
                      handleSubmit(e);
                    }}
                  >
                    Send
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className={tw('mt-30')}>
        <Footer />
      </div>
      <ReactModal
        isOpen={modalOpen}
        onRequestClose={closeModal}
        closeTimeoutMS={500}
        className='Modal'
      >
        <div className={styles.container}>
          <button onClick={closeModal} className={styles.cross}>
            <GrClose />
          </button>
          <div>
            {submitted ? (
              <div>
                <div className={tw('text-purple-600')}>{messageModal}</div>
                <img className={tw('mt-10')} src='images/thank-you.svg' alt='' />
              </div>
            ) : (
              <div>
                <div className={tw('text-red-600')}>{messageModal}</div>
                <img className={tw('mt-10')} src='images/submit-error.svg' alt='' />
              </div>
            )}
          </div>
          <div className={styles.button}>
            <Button variant='uppercase' onClick={closeModal} primary>
              close
            </Button>
          </div>
        </div>
      </ReactModal>
    </>
  );
};

export default contact;
