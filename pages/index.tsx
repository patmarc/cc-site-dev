import { NextSeo } from 'next-seo';
import dynamic from 'next/dynamic';
import Page from '@/components/layout/page';
import Header from '@/components/layout/header';
import VideoSection from '@/components/layout/video-section';
import ListSection from '@/components/layout/list-section';
import FeatureSection from '@/components/layout/feature-section';
import CasesSection from '@/components/layout/cases-section';
// const SocialProof = dynamic(() => import('@/components/layout/social-proof'));
import SocialProof from '@/components/layout/social-proof';
// const PricingTable = dynamic(() => import('@/components/layout/pricing-table'));
import PricingTable from '@/components/layout/pricing-table';
// const Footer = dynamic(() => import('@/components/layout/footer'));
import Footer from '@/components/layout/footer';

export default function Home() {
  return (
    <Page>
      <NextSeo title='CCSite' description='Free music for videos, podcasts, games, applications' />
      <Header />
      <main>
        <VideoSection />
        <ListSection />
        <FeatureSection />
        <CasesSection />
        <SocialProof />
        <PricingTable />
      </main>
      <Footer />
    </Page>
  );
}
