import Meta from '@/components/Meta';
import { getCsrfToken } from 'next-auth/client';
import { tw } from 'twind';
import Footer from '@/components/layout/footer';
import {
  AppleLoginButton,
  GoogleLoginButton,
  FacebookLoginButton,
  TwitterLoginButton,
} from 'react-social-login-buttons';
import Unsplash from 'react-unsplash-wrapper';

const SignIn = ({ csrfToken }) => {
  const socialBtnStyle = { boxShadow: 'none' };

  return (
    <>
      <Meta
        title={'Sign up'}
        keywords={
          'free music for podcasts, free music youtube, free music library, free music for videos, free background music'
        }
        description={'Free Background Music For Your videos, Podcasts, Games, Applications.'}
      />
      <div className={tw('w-full flex flex-wrap pt-24 lg:pt-0 ')}>
        {/* image section */}
        <div className={tw('w-1/2 h-screen hidden md:block')}>
          <Unsplash photoId='fPN1w7bIuNU' width='900' height='900' />
          {/* <img
            className='object-cover w-full h-screen hidden md:block'
            src='https://images.unsplash.com/photo-1592191136579-27d0d2d44e86?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=80'
          /> */}
        </div>
        <div className={tw('w-full md:w-1/2 flex flex-col')}>
          <div
            className={tw(
              'flex flex-col justify-center md:justify-start md:pt-24 md:px-24 bg-gray-50 h-screen'
            )}
          >
            <div className={tw('text-center mb-3')}>
              <h1 className={tw('font-bold text-3xl text-gray-900 uppercase lg:pt-7 lg:pb-3')}>
                Login
              </h1>
              <div>
                <div className='p-1'>
                  <AppleLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
                <div className='p-1'>
                  <GoogleLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
                <div className='p-1'>
                  <TwitterLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
                <div className='p-1'>
                  <FacebookLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
              </div>
            </div>
            <form method='post' action='/api/auth/signin/email'>
              <div className={tw('text-lg mb-2 ml-2')}>
                Enter the email address associated with your account, and we'll send a magic link to
                your inbox.
              </div>
              <input name='csrfToken' type='hidden' defaultValue={csrfToken} />
              <div className={tw('flex -mx-3')}></div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-xs font-semibold px-1'>
                    Email
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='email'
                      id='email'
                      name='email'
                      className={tw(
                        'w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder=''
                    />
                  </div>
                </div>
              </div>

              <div className={tw('mb-3 ml-2 text-sm')}>
                {/* <a href='#'>Forgot your password?</a> */}
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-5')}>
                  <button
                    type='submit'
                    className={tw(
                      'block w-full max-w-xs mx-auto bg-gradient-to-r from-purple-500 to-indigo-500 text-white border-indigo-500 hover:bg-indigo-600 rounded-lg px-3 py-3 font-semibold uppercase'
                    )}
                  >
                    Log in
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className={tw('mt-30')}>
        <Footer />
      </div>
    </>
  );
};

export async function getServerSideProps(context) {
  const csrfToken = await getCsrfToken(context);
  return {
    props: { csrfToken },
  };
}

export default SignIn;
