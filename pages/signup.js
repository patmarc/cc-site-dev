import Meta from '@/components/Meta';
import { getCsrfToken } from 'next-auth/client';
import styles from '@/styles/SignUp.module.css';
import { tw } from 'twind';
import Footer from '@/components/layout/footer';
import {
  AppleLoginButton,
  GoogleLoginButton,
  FacebookLoginButton,
  TwitterLoginButton,
} from 'react-social-login-buttons';
import Unsplash from 'react-unsplash-wrapper';

const SignUp = ({ csrfToken }) => {
  const socialBtnStyle = { boxShadow: 'none' };

  return (
    <>
      <Meta
        title={'Sign up'}
        keywords={
          'free music for podcasts, free music youtube, free music library, free music for videos, free background music'
        }
        description={'Free Background Music For Your videos, Podcasts, Games, Applications.'}
      />
      <div className={tw('w-full flex flex-wrap pt-24 lg:pt-0 ')}>
        {/* image section */}
        <div className={tw('w-1/2 h-screen hidden md:block')}>
          <Unsplash photoId='9XngoIpxcEo' width='900' height='1039' />
          {/* <img
            className='object-cover w-full h-screen hidden md:block'
            src='https://images.unsplash.com/photo-1563089145-599997674d42?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=80'
          /> */}
        </div>
        <div className={tw('w-full md:w-1/2 flex flex-col')}>
          <div
            className={tw(
              'flex flex-col justify-center md:justify-start pt-60 md:pt-24 md:px-24 bg-gray-50'
            )}
          >
            <div className={tw('text-center mb-3')}>
              <h1 className={tw('font-bold text-3xl text-gray-900 uppercase pt-1 pb-2')}>
                Sign up
              </h1>
              <div className={tw('text-center py-2 text-lg')}>
                <p className={tw('text-indigo-600 uppercase font-bold')}>
                  Unlock with your free account
                </p>
                <div className={tw('text-gray-700 pb-2 md:pb-0')}>
                  Add Favorites, Add Ratings, Create Playlists
                </div>
              </div>
              <p className={tw('text-base text-gray-500 font-bold uppercase lg:py-3')}>
                Sign up with your Social Account
              </p>
              <div>
                <div className='p-1'>
                  <AppleLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
                <div className='p-1'>
                  <GoogleLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
                <div className='p-1'>
                  <TwitterLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
                <div className='p-1'>
                  <FacebookLoginButton style={socialBtnStyle} onClick={() => alert('Hello')} />
                </div>
              </div>
              <div className={`${styles.orLine}`}>or</div>
            </div>
            <form method='post' action='/api/auth/signin/email'>
              <div className='py-2 text-base text-gray-500 font-bold uppercase text-center'>
                <p>Create Your Free Account</p>
              </div>
              <input name='csrfToken' type='hidden' defaultValue={csrfToken} />
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className={tw('text-xs font-semibold px-1')}>
                    Username
                  </label>
                  <div className='flex'>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-account-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='text'
                      className={tw(
                        'w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder=''
                    />
                  </div>
                </div>
              </div>
              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 mb-3')}>
                  <label htmlFor='' className='text-xs font-semibold px-1'>
                    Email
                  </label>
                  <div className={tw('flex')}>
                    <div
                      className={tw(
                        'w-10 z-10 pl-1 text-center pointer-events-none flex items-center justify-center'
                      )}
                    >
                      <i className={tw('mdi mdi-email-outline text-gray-400 text-lg')}></i>
                    </div>
                    <input
                      type='email'
                      id='email'
                      name='email'
                      className={tw(
                        'w-full -ml-10 pl-5 pr-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500'
                      )}
                      placeholder=''
                    />
                  </div>
                </div>
              </div>

              <div className={tw('flex w-full px-2 my-3 md:font-sans text-sm text-gray-800')}>
                <input type='checkbox' className={tw('mr-2 inline-block border-0 self-center')} />
                <span display='inline'>
                  By creating an account you are agreeing to our
                  <a href='/s/terms' target='_blank' data-test='Link'>
                    <span className={tw('underline mx-1')}>Terms and Conditions</span>
                  </a>
                  and
                  <a href='/s/privacy' target='_blank' data-test='Link'>
                    <span className={tw('underline mx-1')}>Privacy Policy</span>
                  </a>
                </span>
              </div>

              <div className={tw('flex -mx-3')}>
                <div className={tw('w-full px-3 py-4 mb-20')}>
                  <button
                    type='submit'
                    className={tw(
                      'block w-full max-w-xs mx-auto bg-gradient-to-r from-purple-500 to-indigo-500 text-white border-indigo-500 hover:bg-indigo-600 rounded-lg px-3 py-3 font-semibold uppercase'
                    )}
                  >
                    Create account
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className={tw('mt-30')}>
        <Footer />
      </div>
    </>
  );
};

export async function getServerSideProps(context) {
  const csrfToken = await getCsrfToken(context);
  return {
    props: { csrfToken },
  };
}

export default SignUp;
