import { useState } from 'react';
import { server } from '../../config';
import { useRouter } from 'next/router';
import Meta from '../../components/Meta';
import ReactModal from 'react-modal';
import { useStore } from '@/store/Store';
import { GrClose } from 'react-icons/gr';
import Modal from 'react-bootstrap/Modal';
import Button from '@/components/layout/button';
import { tw } from 'twind';
import Unsplash from 'react-unsplash-wrapper';
import PlayBtn from '@/components/PlayBtn';
import Favorite from '@/components/player/Favorite';
import dynamic from 'next/dynamic';
// importing external modules depending on window:
// https://nextjs.org/docs/advanced-features/dynamic-import#with-no-ssr
const ReactWaves = dynamic(() => import('@dschoon/react-waves'), { ssr: false });

const track = ({ track }) => {
  const { setModalDetails, playing, setPlaying } = useStore();
  const [loading, setLoading] = useState(true);

  const router = useRouter();

  const openModal = () => {
    setModalDetails(true);
  };
  const closeModal = () => {
    setModalDetails(false);
    router.push('/browse');
    setPlaying(false);
  };

  ReactModal.setAppElement('body');

  return (
    <>
      <Meta title={track.title} keywords={track.tag_name} description={track.description} />

      <ReactModal
        isOpen={true}
        onRequestClose={closeModal}
        className={tw('bg-white md:pt-3')}
        closeTimeoutMS={500}
      >
        <div className={tw('w-full flex flex-wrap px-6 md:pt-24 lg:pt-0 h-screen')}>
          <div className={tw('w-1/2 hidden md:block')}>
            {/* Skeleton loader */}
            {/* <div className={tw('h-32 bg-gray-200 rounded-tr rounded-tl animate-pulse')}></div> */}

            <Unsplash width='900' height='900' keywords={track.title.split(' ').join(',')}>
              <h1
                style={{
                  fontFamily: 'helvetica',
                  fontWeight: 'bold',
                  textTransform: 'uppercase',
                  fontSize: '3rem',
                  color: '#000',
                  background: 'rgb(255 255 255 / 78%)',
                  padding: '20px 40px 20px 40px',
                }}
              >
                {track.title}
              </h1>
            </Unsplash>
          </div>
          <div
            className={tw(
              'w-full md:w-1/2 flex flex-col justify-center md:justify-start md:pt-3 md:px-10 overflow-scroll h-screen'
            )}
          >
            <h1 className={tw('text-4xl mb-4')}>{track.title}</h1>
            <ReactWaves
              audioFile={track.audio_url}
              className={'react-waves'}
              options={{
                barHeight: 2,
                cursorWidth: 0,
                height: 100,
                hideScrollbar: true,
                progressColor: '#a78bfa',
                responsive: true,
                waveColor: '#D1D6DA',
              }}
              volume={1}
              zoom={1}
              playing={playing}
            />
            <div className={tw('flex justify-between mb-7')}>
              <PlayBtn id={track.track_id} />

              <div className={tw('flex self-center text-2xl')}>
                <Favorite id={parseInt(track.track_id)} />
              </div>
            </div>
            <p className={tw('text-lg mb-4')}>{track.description}</p>
            <p className={tw('text-base mb-2')}>
              <span className={tw('font-bold')}>Author: </span>
              {track.author_name}
            </p>
            <p className={tw('text-base mb-2')}>
              <span className={tw('font-bold')}>Genre: </span>
              {track.genre_name}
            </p>
            <p className={tw('text-base mb-2')}>
              <span className={tw('font-bold')}>Duration: </span>
              {track.duration}
            </p>
            <p className={tw('text-base mb-2')}>
              <span className={tw('font-bold')}>Tempo: </span>
              {track.tempo}
            </p>
            <p className={tw('text-base mb-2')}>
              <span className={tw('font-bold')}>Tags: </span>
              {track.tag_name.join(', ')}
            </p>
            <p className={tw('text-base mb-2')}>
              <span className={tw('font-bold')}>License: </span>
            </p>
            <div></div>
            <Button variant='uppercase' onClick={closeModal} primary>
              Close
            </Button>
          </div>
        </div>
      </ReactModal>
    </>
  );
};

export const getServerSideProps = async ({ params }) => {
  const res = await fetch(`${server}/api/tracks/${params.id}`);
  const track = await res.json();
  if (!track) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      track,
    },
  };
};

// export const getStaticPaths = async () => {
//   const res = await fetch(`${server}/api/tracks`);
//   const tracks = await res.json();

//   const paths = tracks.map((track) => ({
//     params: { id: track.track_id },
//   }));
//   return { paths, fallback: true };
// };

// export const getStaticPaths = async () => {
//   const res = await fetch(`${server}/api/tracks`);
//   const tracks = await res.json();

//   const ids = tracks.map((track) => track.track_id);
//   const paths = ids.map((id) => ({ params: { id: id.toString() } }));

//   return {
//     paths,
//     fallback: true,
//   };
// };

export default track;
