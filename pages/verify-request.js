import { tw } from 'twind';

import {
  GoogleLoginButton,
  FacebookLoginButton,
  TwitterLoginButton,
} from 'react-social-login-buttons';

const SignIn = ({ csrfToken }) => {
  const socialBtnStyle = { boxShadow: 'none' };

  return (
    <>
      <div className={tw('w-full flex flex-wrap pt-24 lg:pt-0 ')}>
        {/* image section */}
        <div className={tw('w-1/2 md:w-1/2 shadow-2xl')}>
          <img
            className={tw('object-cover w-full h-screen hidden md:block')}
            src='https://images.unsplash.com/photo-1592191136579-27d0d2d44e86?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2023&q=80'
          />
        </div>
        <div className={tw('w-full md:w-1/2 flex flex-col')}>
          <div
            className={tw(
              'flex flex-col justify-center md:justify-start md:pt-24 md:px-24 bg-gray-50 h-screen'
            )}
          >
            <div className={tw('text-center mb-3 font-bold text-purple-600 uppercase lg:py-5')}>
              <h1 className={tw('text-3xl mb-3')}>Check your email!</h1>
              <h3>A sign in link has been sent to your email address.</h3>
              <img className={tw('mt-10')} src='images/link-email.svg' alt='' />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SignIn;
