import { createContext, useContext, useState, useRef } from 'react';

const Store = createContext();
Store.displayName = 'Store';

export const useStore = () => useContext(Store);

export const StoreProvider = ({ children }) => {
  const [playing, setPlaying] = useState(false);
  const [idTrack, setIdTrack] = useState(0);
  const [volume, setVolume] = useState(0.8);
  const [played, setPlayed] = useState(0);
  const [url, setUrl] = useState(null);
  const [title, setTitle] = useState(null);
  const [seeking, setSeeking] = useState(false);
  const [loaded, setLoaded] = useState(0);
  const [playbar, setPlaybar] = useState(false);
  const [home, setHome] = useState(true);
  const [favoriteView, setFavoriteView] = useState(false);
  const [playlistView, setPlaylistView] = useState(false);
  const [cbCheck, setCBCheck] = useState([]);
  const [durationValues, setDurationValues] = useState([]);
  const [tempoValues, setTempoValues] = useState([]);
  const [modalSignInOpen, setModalSignInOpen] = useState(false);
  const [modalSignUpOpen, setModalSignUpOpen] = useState(false);
  const [modalSocialOpen, setModalSocialOpen] = useState(false);
  const [modalDetails, setModalDetails] = useState(true);
  const [modalSocialData, setModalSocialData] = useState([]);
  const [modalRatingOpen, setModalRatingOpen] = useState(false);
  const [ratingComment, setRatingComment] = useState('');
  const [selectedTags, setSelectedTags] = useState([]);
  const [selectedAuthors, setSelectedAuthors] = useState([]);
  const [search, setSearch] = useState('');
  const [msg, setMsg] = useState(false);
  const [favorite, setFavorite] = useState([]);
  const [favoriteChanged, SetFavoriteChanged] = useState(false);
  const refPlayer = useRef();

  return (
    <Store.Provider
      value={{
        playing,
        setPlaying,
        idTrack,
        setIdTrack,
        volume,
        setVolume,
        played,
        setPlayed,
        url,
        setUrl,
        title,
        setTitle,
        seeking,
        setSeeking,
        loaded,
        setLoaded,
        playbar,
        setPlaybar,
        home,
        setHome,
        favoriteView,
        setFavoriteView,
        playlistView,
        setPlaylistView,
        cbCheck,
        setCBCheck,
        durationValues,
        setDurationValues,
        tempoValues,
        setTempoValues,
        modalSignInOpen,
        setModalSignInOpen,
        modalSignUpOpen,
        setModalSignUpOpen,
        modalDetails,
        setModalDetails,
        modalSocialOpen,
        setModalSocialOpen,
        modalSocialData,
        setModalSocialData,
        modalRatingOpen,
        setModalRatingOpen,
        ratingComment,
        setRatingComment,
        selectedTags,
        setSelectedTags,
        selectedAuthors,
        setSelectedAuthors,
        search,
        setSearch,
        msg,
        setMsg,
        favorite,
        setFavorite,
        favoriteChanged,
        SetFavoriteChanged,
        refPlayer,
      }}
    >
      {children}
    </Store.Provider>
  );
};
